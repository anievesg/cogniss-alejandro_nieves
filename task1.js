const jsonfile = require('jsonfile');
const randomstring = require('randomstring');
const file = 'input2.json';

let data = {};
let fakeEmails = [];

function reverse(str) {
    var splitString = str.split('');
    var reverseArray = splitString.reverse();
    var joinArray = reverseArray.join('');

    return joinArray;
}

jsonfile.readFile(file, (err, obj) => {
    if (err) console.error(err);
    data = obj;

    data.names.forEach(name => {
        // 1 - Reverse name
        let fakeEmail = reverse(name);

        // 2 - Concat random 5 characters and @gmail.com
        fakeEmail = fakeEmail + randomstring.generate(5) + '@gmail.com';

        fakeEmails = fakeEmails.concat(fakeEmail);
    });

    var output = { emails: fakeEmails }

    jsonfile.writeFile('output2.json', output, (err) => {
        if (err) console.error(err);
    });
});
